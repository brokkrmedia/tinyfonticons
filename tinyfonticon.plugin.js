tinymce.PluginManager.add("tinyfonticon", function(editor, url) {
  editor.on("init", function() {
    var configs = tinyMCE.activeEditor.getParam("tinyfonticon");
    for (var i in configs){
      var stylesheet = document.getElementById(i);
      tinyMCE.DOM.loadCSS(stylesheet.href);
      editor.dom.loadCSS(stylesheet.href);
    }
  });
  var styles = [];
  var configs = tinyMCE.activeEditor.getParam("tinyfonticon");
  for (var i in configs){
    styles[configs[i]['font']] = getFontIconStyles(editor, i, configs[i]["prefix"], configs[i]["added_class"] || '');
  }
  var list = "";
  for (var i in styles){
    list = list.concat(assembleList(styles[i], i));
  }
  editor.addButton("tinyfonticon", {
    type: "panelbutton",
    text: 'tfi',
    tooltip: "Tiny Font Icons",
    panel: {
      role: "application",
      autohide: !0,
      html: list,
      onclick: function(a) {
        if(a.target.nodeName == 'SPAN') {
          editor.insertContent(a.target.outerHTML, this.hide());
        }
      }
    },
  })

  function getFontIconStyles(editor, sheetId, prefix, added_class, font){
    var stylesheet = document.getElementById(sheetId);
    var fontRules = stylesheet.sheet.rules || stylesheet.sheet.cssRules;
    var styles = [];
    for(var x=0;x<fontRules.length;x++) {
      selectors = fontRules[x].selectorText;
      if (typeof(selectors) != "undefined" && fontRules[x].style.cssText.indexOf("content") > -1){
        if (selectors.indexOf(prefix) === 0) {
          selectors = selectors.split(",");
          for(var y=0;y<selectors.length;y++){
            styles[styles.length] = cleanSelector(selectors[y]) + " " + added_class + "|" + cleanSelector(fontRules[x].style.cssText);
          }
        }
      }
    }
    return styles;
  }

  function assembleList(styles, font){
    var total = styles.length;
    var increment = 40;
    var list =  '<div>' + font + '</div><table role="list" class="mce-grid">';
    for(var x=0;x<styles.length;x++) {
      var values = styles[x].split('|');
      var item = '<td><span style="font-family:' + font + ';" fontclass="' + 
          values[0] + '" >' + 
          values[1] + '</span></td>';
      if((x)%increment == 0){
        list += '<tr>';
      }
      list += item;
      if((x)%increment == increment-1){
        list += '</tr>';
      }
    }
    list +=  '</table>';
    return list;
  }

  function cleanSelector(selector){
    return selector.replace('::before', '').replace(':before', '').replace('::after', '').replace('.', '').replace('content:', '').replace(';', '').replace('\'', '').replace('\'', '');
  }

  function fonticonSaveContent(element_id, html, body) {
    console.log(html);
    return html;
  }
});
