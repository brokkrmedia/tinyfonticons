# TinyFontIcon(Beta) #

This plugin enables you to use font icons inside TinyMCE

## Setup ##

Your fonticon files must be loaded onto the page where TinyMCE is being used.

Tag the links with ids:

```
#!html

<link rel="stylesheet" id="fontello_style" href="fontello/css/fontello.css">
<link rel="stylesheet" id="fontawesome_style" href="font-awesome/css/font-awesome.css">

```

Then load the plugin with TinyMCE with TinyFontIcon settings for each font type:

```
#!javascript

tinymce.init({
  selector: "textarea",
  plugins: "tinyfonticon",
  toolbar: "tinyfonticon",
  tinyfonticon: {
    fontello_style : {
      prefix: ".icon-",
    },
    fontawesome_style : {
      prefix: ".fa-",
      added_class : "fa",
    }
  },
  extended_valid_elements : "icon[class]"
});
```


The array key for each tinyfonticon setting should match the id you attributed to the font icon links

Prefix is what how all the css rules start

Added class is any classes that needed to be added to the element for that particular font.

You have to add the below valid elements:

```
#!javascript

extended_valid_elements : "icon[class]"
```